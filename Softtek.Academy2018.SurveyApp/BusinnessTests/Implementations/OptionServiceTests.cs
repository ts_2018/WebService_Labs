﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Businness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Data.Contracts;
using Moq;
using Softtek.Academy2018.SurveyApp.Domain.Model;

namespace Businness.Tests
{
    [TestClass()]
    public class OptionServiceTests
    {
        [TestMethod()]
        public void AddTest()
        {
            int expected = 1;

            Mock<IOptionRepository> repository = new Mock<IOptionRepository>();
            repository.Setup(x => x.Add(It.IsAny<Option>())).Returns(1);
            IOptionService service = new OptionService(repository.Object);            

            Option opt = new Option
            {
                Text= "Opcion1",                
                ModifiedDate = DateTime.Now,                 
                CreatedDate = DateTime.Now
            };

            int result = service.Add(opt);

            Assert.AreEqual(expected, result); 
             
        }

        [TestMethod()]
        public void GetAllTest()
        {
            Mock<IOptionRepository> repository = new Mock<IOptionRepository>();
            ICollection<Option> options = new List<Option>();
            //options.Count = 10;
            repository.Setup(x => x.GetAll()).Returns(options);
            //repository.Setup(x => x.GetAll().Count()).Returns(10);

            IOptionService service = new OptionService(repository.Object);

            ICollection<Option> opciones = service.GetAll();
            

            int size = 0;
            if(opciones != null)
                size = opciones.Count();

            Assert.IsNotNull(opciones);

        }




        }
}