﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using Softtek.Academy2018.SurveyApp.Data.Contracts;

namespace Businness
{
    public class OptionService : IOptionService
    {
        private readonly IOptionRepository _optionRepository;

        public OptionService(IOptionRepository optionRepository)
        {
            this._optionRepository = optionRepository;
        }

        public int Add(Option entity)
        {
            if (entity == null) return 0;

            if (entity.Id != 0) return 0;
            if (string.IsNullOrEmpty(entity.Text)) return 0;
            if (entity.Questions == null || entity.Questions.Count() < 0) return 0;
            if (entity.CreatedDate == null ) return 0;
            if (entity.ModifiedDate == null ) return 0;

            int id = _optionRepository.Add(entity);
            return id;
        }

        public ICollection<Option> GetAll()
        {
            ICollection<Option>  opciones = _optionRepository.GetAll();
            return opciones;
        }

        public bool Update(Option entity)
        {
            //Una opción no puede ser modificada si es parte de una pregunta
            throw new NotImplementedException();
        }
    }
}
