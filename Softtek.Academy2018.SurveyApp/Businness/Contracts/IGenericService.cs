﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Businness.Contracts
{
    public interface IGenericService<T> where T : class
    {
        int Add(T entity);        

        bool Update(T entity);

        ICollection<T> GetAll();


    }
}
