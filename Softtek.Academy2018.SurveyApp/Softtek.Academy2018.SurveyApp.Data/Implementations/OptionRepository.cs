﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class OptionRepository : IOptionRepository
    {
        public int Add(Option entity)
        {
            if (entity == null) return -1;

            using (var context = new SuerveyDbContext())
            {                
                context.Options.Add(entity);
                context.SaveChanges();
            }

            

            return entity.Id;
        }


        public bool Update(Option entity)
        {
            if (entity == null) return false;
            using (var context = new SuerveyDbContext())
            {

                Option opt = context.Options.SingleOrDefault(opt2 => opt2.Id == entity.Id);
                if (opt == null) return false;

                opt.Id = entity.Id;
                opt.Text = entity.Text;
                opt.CreatedDate = DateTime.Now;
                opt.ModifiedDate = DateTime.Now;
                opt.Questions = entity.Questions;                               

                context.SaveChanges();
            }
            return true;
        }


        public ICollection<Option> GetAll()
        {
            ICollection<Option> options = new List<Option>();
            using (var context = new SuerveyDbContext())
            {
                options = context.Options.ToList();
            }

            return options;

        }

        public Option Get(int id)
        {
            throw new NotImplementedException();
        } 

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public bool Exist(string v)
        {
            using (var ctx = new SuerveyDbContext())
            {
                return ctx.Options.AsNoTracking().Any(x => x.Text.ToLower() == v.ToLower());
            }
        }
    }
}
