﻿using Businness;
using MyNewWebAPI.Models;
using Softtek.Academy2018.SurveyApp.Data.Contracts;
using Softtek.Academy2018.SurveyApp.Data.Implementations;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyNewWebAPI.Controllers
{
    [RoutePrefix("api/options")]
    public class OptionController : ApiController
    {
        private readonly IOptionService _optionService;

        public OptionController()//IOptionService optionService)
        {

            IOptionRepository _optionService = new OptionRepository();
            this._optionService = new OptionService(_optionService);
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult Create([FromBody] OptionDTO optionDTO)
        {
            if (optionDTO == null) return BadRequest(); 
             
            Option option = new Option
            {                
                Text = optionDTO.Text,
                CreatedDate = DateTime.Now,
                ModifiedDate = DateTime.Now
            };

            int id = _optionService.Add(option);

            if (id <= 0) return BadRequest("Unable to create option");

            var payload = new { ProjectId = id }; //objeto dinamico

            return Ok(payload);             
        }



        [Route("all")]
        [HttpGet]
        public IHttpActionResult GetAll()
        {
            ICollection<Option> options = _optionService.GetAll();

            if (options == null || options.Count() <= 0) return NotFound();

            ICollection<OptionDTO> optionDTOs = options.Select(pj => new OptionDTO
            {
                Text = pj.Text
            }).ToList();

            return Ok(optionDTOs);
        } 

    }
}
